import { ChargePage } from './../pages/charge/charge';
import { NewCustomerPage } from './../pages/new-customer/new-customer';
import { ScanPage } from './../pages/scan/scan';
import { ServicesPage } from './../pages/services/services';
import { LoginPage } from './../pages/login/login';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ApiProvider } from '../providers/api/api';
import { CustomerExitPage } from '../pages/customer-exit/customer-exit';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    ServicesPage,
    ScanPage,
    NewCustomerPage,
    ChargePage,
    CustomerExitPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ServicesPage,
    ScanPage,
    NewCustomerPage,
    ChargePage,
    CustomerExitPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
    BarcodeScanner
  ]
})
export class AppModule {}
