import { Injectable } from '@angular/core';
import Axios from 'axios';
import { ENV } from '../../app/app.constant';

@Injectable()
export class ApiProvider {

  constructor() {
    console.log('Hello ApiProvider Provider');
    /* Axios Defualt Variables */
    Axios.defaults.baseURL = 'https://rezghi.herokuapp.com/parse';
    Axios.defaults.headers.common ["X-Parse-Application-Id"] = "myApp";
    Axios.defaults.headers.post ["Content-Type"] = "application/json";  
  }

  login (username, password) {
    return Axios.get('/login', {
      params: {
        username: username,
        password: password
      },
      headers: {
        "X-Parse-Revocable-Session": "1",
        "X-Parse-Application-Id": "myApp",
        "Content-Type": "application/json"
      }
    });
  }

  signup(id,phone,code,name,amount) {
    return Axios.put('/users/'+id, {
      name: name,
      national_code: code,
      amount: parseInt(amount),
      username: phone
    });
  }

  newCustomer() {
    var timestamp = new Date().getUTCMilliseconds();
    return Axios.post('/users', {
      username: timestamp.toString(),
      password: '123456',
      isEmployee: false
    });
  }

  getServices() {
    return Axios.get('/classes/Services');
  }

  expence(amount, service, unit, customer) {
    return Axios.post('/classes/Expence', {
      amount: parseInt(amount),
      unit: parseInt(unit),
      creator: {
        "__type":"Pointer",
        "className":"_User",
        "objectId": ENV.id
      },
      service: {
        "__type":"Pointer",
        "className":"Services",
        "objectId": service
      },
      customer: {
        "__type":"Pointer",
        "className":"_User",
        "objectId": customer
      }
    });
  }

  findCard(id) {
    return Axios.get('/classes/Cards/'+id);
  }

  emptyCard(id) {
    return Axios.put('/classes/Cards/'+id, {
      amount: 0,
      customer: {
        "__type":"Pointer",
        "className":"_User",
        "objectId": null
      },
      active: false
    });
  }

  assignCard(id, amount, customer) {
    return Axios.put('/classes/Cards/'+id, {
      amount: parseInt(amount),
      customer: {
        "__type":"Pointer",
        "className":"_User",
        "objectId": customer
      },
      active: true
    });
  }

  Income(amount, customer, isCash, RefId) {
    return Axios.post('/classes/Income', {
      amount: parseInt(amount),
      creator: {
        "__type":"Pointer",
        "className":"_User",
        "objectId": ENV.id
      },
      isCash: isCash,
      RefId: RefId,
      customer: {
        "__type":"Pointer",
        "className":"_User",
        "objectId": customer
      }
    });
  }

  Increment(card, amount) {
    return Axios.put('/classes/Cards/'+card, {
      amount: {
        "__op": "Increment",
        "amount": parseInt(amount)
      }
    });
  }

  Decrement(card, amount) {
    return Axios.put('/classes/Cards/'+card, {
      amount: {
        "__op": "Increment",
        "amount": -parseInt(amount)
      }
    });
  }

}
