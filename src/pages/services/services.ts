import { HomePage } from './../home/home';
import { ApiProvider } from './../../providers/api/api';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-services',
  templateUrl: 'services.html',
})
export class ServicesPage {

  card = {
    amount: 0,
    active: false,
    customer: {
      "__type":"Pointer",
      "className":"_User",
      "objectId": ''
    },
    objectId: '',
    creator: {}
  };
  services;
  unit;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public api: ApiProvider) {
    api.getServices().then(res => {
      this.services = res.data.results;
    });
    api.findCard(navParams.get('id')).then(res => { 
      this.card.amount = res.data.amount;
      this.card.active = res.data.active;
      this.card.customer = res.data.customer;
      this.card.objectId = res.data.objectId;
    });
  }

  ionViewDidLoad() {}

  next(id, amount, unit) {
    var total = parseInt(unit) * parseInt(amount);
    console.log('ID: '+ id + '| Amount: ' + amount + '| Unit: ' + unit);
    console.log(total);
    if(this.card.amount >= total) {
      this.api.expence(total, id, unit, this.card.customer.objectId)
      .then(res => {
        if(res.status == 201){
          this.api.Decrement(this.card.objectId, total)
          .then(re => {
            this.alertCtrl.create({
              title: 'با موفقیت ثبت شد',
              buttons: ['بستن']
            }).present();
            this.navCtrl.push(HomePage);
          });
        }
      });
    }
    else {
      this.alertCtrl.create({
        title: 'موجودی کارت کافی نیست',
        buttons: ['بستن']
      }).present();
      this.navCtrl.push(HomePage);
    };
  }

}