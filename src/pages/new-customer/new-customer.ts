import { ScanPage } from './../scan/scan';
import { HomePage } from './../home/home';
import { ApiProvider } from './../../providers/api/api';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-new-customer',
  templateUrl: 'new-customer.html',
})
export class NewCustomerPage {

  card = {
    amount: 0,
    active: false,
    customer: {},
    objectId: '',
    creator: {}
  };
  amount;
  isCash;
  RefId;

  constructor(public alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams,public api: ApiProvider) {
    api.findCard(navParams.get('id')).then(res => { 
      this.card.amount = res.data.amount;
      this.card.active = res.data.active;
      this.card.customer = res.data.customer;
      this.card.objectId = res.data.objectId;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewCustomerPage');
  }

  charge() {
    this.api.newCustomer().then(res => {
      if(res.status == 201) {
        this.api.Income(this.amount,res.data.objectId,this.isCash,this.RefId).then(r =>{
          if(r.status == 201) {
            this.api.assignCard(this.card.objectId, this.amount, res.data.objectId).then(re => {
              let alert = this.alertCtrl.create({
                title: 'کارت با موفقیت فعال شد',
                buttons: ['بستن']
              });
              alert.present();
              this.navCtrl.push(HomePage);
            });
          }
        });
      }
    });
  }

  emptyCard() {
    let alert = this.alertCtrl.create({
      title: 'صفر کردن موجودی کارت',
      message: 'آیا اطمینان دارید؟',
      buttons: [
        {
          text: 'اسکن کارت دیگر',
          handler: () => {
            this.navCtrl.push(ScanPage,{id:0});
          }
        },
        {
          text: 'بله',
          handler: () => {
            this.api.expence(this.card.amount, '', 0, '').then(res => {
              if (res.status == 201){
                this.api.emptyCard(this.card.objectId).then(r => {
                  this.card.amount = 0;
                  this.card.active = false;
                  let alert = this.alertCtrl.create({
                    title: 'کارت با موفقیت غیرفعال شد',
                    buttons: ['بستن']
                  });
                  alert.present();
                  this.navCtrl.push(NewCustomerPage,{id:this.card.objectId});
                });
              }
            });
          }
        },
        {
          text: 'انصراف',
          handler: () => {

          }
        }
      ]
    });
    alert.present();
  }

}
