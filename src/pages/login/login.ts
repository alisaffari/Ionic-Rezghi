import { HomePage } from './../home/home';
import { ApiProvider } from './../../providers/api/api';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ENV } from '../../app/app.constant';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [ApiProvider]
})
export class LoginPage {

  username: string;
  password: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public api: ApiProvider, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login() {
    this.api.login(this.username, this.password)
    .then(res => {
      if(res.status == 200) {
        ENV.id = res.data.objectId;
        this.navCtrl.push(HomePage);
      }
    })
    .catch(err => {
      console.log(err);
      let alert = this.alertCtrl.create({
        title: 'خطا',
        subTitle: 'اطلاعات ورود اشتباه است',
        buttons: ['بستن']
      });
      alert.present();
    });
  }

}
