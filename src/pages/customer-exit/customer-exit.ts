import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-customer-exit',
  templateUrl: 'customer-exit.html',
})
export class CustomerExitPage {

  card = {
    amount: 0,
    active: false,
    customer: {
      "__type":"Pointer",
      "className":"_User",
      "objectId": ''
    },
    objectId: '',
    creator: {}
  };
  name;
  phone;
  code;
  amount;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public api: ApiProvider) {
                api.findCard(navParams.get('id')).then(res => { 
                  this.card.amount = res.data.amount;
                  this.card.active = res.data.active;
                  this.card.customer = res.data.customer;
                  this.card.objectId = res.data.objectId;
                });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CustomerExitPage');
  }

  signup() {
    this.api.signup(this.card.customer.objectId, this.phone,this.code,this.name,this.amount)
    .then(res => {
      this.api.expence(this.card.amount, '', 0, '').then(re => {
        if (re.status == 201){
          this.api.emptyCard(this.card.objectId).then(r => {
            this.card.amount = 0;
            this.card.active = false;
            this.alertCtrl.create({
              title: 'با موفقیت ثبت شد',
              buttons: ['بستن']
            }).present()
            this.navCtrl.push(HomePage);
          })
        }
    })
  })
    .catch(err => {
      console.log(err);
      this.alertCtrl.create({
        title: 'با خطا مواجه شد',
        buttons: ['بستن']
      }).present()
      this.navCtrl.push(HomePage);
    })
  }

  deactive() {
    let alert = this.alertCtrl.create({
      title: 'صفر کردن موجودی کارت',
      message: 'آیا اطمینان دارید؟',
      buttons: [
        {
          text: 'بله',
          handler: () => {
            this.api.expence(this.card.amount, '', 0, '').then(res => {
              if (res.status == 201){
                this.api.emptyCard(this.card.objectId).then(r => {
                  this.card.amount = 0;
                  this.card.active = false;
                  let alert = this.alertCtrl.create({
                    title: 'کارت با موفقیت غیرفعال شد',
                    buttons: ['بستن']
                  });
                  alert.present();
                  this.navCtrl.push(HomePage);
                });
              }
            });
          }
        },
        {
          text: 'انصراف',
          handler: () => {

          }
        }
      ]
    });
    alert.present();
  }

}
