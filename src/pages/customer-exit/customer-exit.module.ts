import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomerExitPage } from './customer-exit';

@NgModule({
  declarations: [
    CustomerExitPage,
  ],
  imports: [
    IonicPageModule.forChild(CustomerExitPage),
  ],
})
export class CustomerExitPageModule {}
