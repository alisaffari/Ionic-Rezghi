import { ChargePage } from './../charge/charge';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { NewCustomerPage } from '../new-customer/new-customer';
import { ServicesPage } from '../services/services';
import { CustomerExitPage } from '../customer-exit/customer-exit';

@IonicPage()
@Component({
  selector: 'page-scan',
  templateUrl: 'scan.html',
})
export class ScanPage {

  State;
  Page;
  barcode = '';

  constructor(public alertCtrl: AlertController,
              public navCtrl: NavController, 
              public navParams: NavParams, 
              public barcodeScanner: BarcodeScanner) {
    this.State = navParams.get('id');
    if (this.State == 0){ this.Page = NewCustomerPage; }
    else if(this.State == 1){ this.Page = ServicesPage; }
    else if(this.State == 2){ this.Page = ChargePage; }
    else if(this.State == 3){ this.Page = CustomerExitPage; };
  }

  scan() {
    this.barcodeScanner.scan().then(data => { 
      this.barcode = data.text;
      this.goto();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ScanPage');
  }

  goto() {
    this.navCtrl.push(this.Page,{ id: this.barcode});
  }

  go() {
    this.navCtrl.push(this.Page,{ id: 'uMT6QfcDgF'});
  }

}
