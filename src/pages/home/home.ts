import { ScanPage } from './../scan/scan';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  gotoser() {
    this.navCtrl.push(ScanPage, {id:1});
  }

  newCustomer() {
    this.navCtrl.push(ScanPage, {id:0});
  }

  charge() {
    this.navCtrl.push(ScanPage, {id:2});
  }

  exit() {
    this.navCtrl.push(ScanPage, {id:3});
  }

}
