import { HomePage } from './../home/home';
import { ApiProvider } from './../../providers/api/api';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-charge',
  templateUrl: 'charge.html',
})
export class ChargePage {

  card = {
    amount: 0,
    active: false,
    customer: {
      "__type":"Pointer",
      "className":"_User",
      "objectId": ''
    },
    objectId: '',
    creator: {}
  };
  amount;
  isCash;
  RefId;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public api: ApiProvider) {
                api.findCard(navParams.get('id')).then(res => { 
                  this.card.amount = res.data.amount;
                  this.card.active = res.data.active;
                  this.card.customer = res.data.customer;
                  this.card.objectId = res.data.objectId;
                });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChargePage');
  }

  charge() {
    this.api.Income(this.amount, this.card.customer.objectId, this.isCash, this.RefId)
    .then(res => {
      if(res.status == 201){
        this.api.Increment(this.card.objectId, this.amount)
        .then(re => {
          this.alertCtrl.create({
            title: 'با موفقیت ثبت شد',
            buttons: ['بستن']
          }).present();
          this.navCtrl.push(HomePage);
        })
        .catch(err => {
          console.log(err);
          this.alertCtrl.create({
            title: 'عملیات با خطا مواجه شد',
            buttons: ['بستن']
          }).present();
          this.navCtrl.push(HomePage);
        });
      }
    });
  }

}
